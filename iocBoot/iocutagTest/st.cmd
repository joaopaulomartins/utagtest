#!../../bin/linux-x86_64/utagTest

#- You may have to change utagTest to something else
#- everywhere it appears in this file

< envPaths

epicsEnvSet(EPICS_CA_ADDR_LIST, "172.30.7.255")
epicsEnvSet(EPICS_PVA_ADDR_LIST, "172.30.7.255")
epicsEnvSet(EPICS_CA_AUTO_ADDR_LIST, "NO")
epicsEnvSet(EPICS_PVA_AUTO_ADDR_LIST, "NO")

epicsEnvSet(EVR_DEV, "LAB-010:Ctrl-EVR-101:")
#epicsEnvSet(EVR_TAG, "EvtACnt-I")


## Register all support components
dbLoadDatabase("../../dbd/utagTest.dbd",0,0)
utagTest_registerRecordDeviceDriver(pdbbase) 

## Load record instances
dbLoadRecords("../../db/utagtest.template","P=UTAGTEST:,R=,EVR_DEV=$(EVR_DEV)")

iocInit()


